#!/bin/sh

ICONlow="🔈"
ICONmid="🔉"
ICONhigh="🔊"
ICONmute="🔇"
ICONspeakermute="🔕"
ICONspeaker="🔔"
ICONheadphone="🎧"

SINKUSB=alsa_output.usb-Harman_Multimedia_JBL_Pebbles_1.0.0-00.analog-stereo

SINK="$(pactl get-default-sink)"
VOLUME=$(pactl get-sink-volume @DEFAULT_SINK@ | awk '{printf $5}' | sed s/%//)
MUTE=$(pactl get-sink-mute @DEFAULT_SINK@ | awk '{printf $2}')

getIcon() {
    if [ "$MUTE" = "yes" ]; then
	ICON=$ICONmute
    else
	if [ "$VOLUME" -gt "50" ]; then
	    ICON=$ICONhigh
	elif [ "$VOLUME" -gt "20" ]; then
	    ICON=$ICONmid
	else
	    ICON=$ICONlow
	fi
    fi

    if [ "$SINK" = "$SINKUSB" ]; then
	printf "$ICON %s" "$VOLUME%"
    else
	printf "$ICON %s" "$VOLUME% $ICONheadphone"
    fi
}

getIcon
